import { SEARCH_USERS, SEARCH_USERS_ERROR } from 'actions/types';

const initialState = {
  data: null,
  error: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SEARCH_USERS:
      return {
        ...state,
        data: action.data
      }
    case SEARCH_USERS_ERROR:
      return {
        state,
        error: action.error
      }
    default:
      return state;
  }
}
