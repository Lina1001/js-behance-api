import { GET_PROJECTS, GET_PROJECTS_ERROR } from 'actions/types';

const initialState = {
  data: null,
  error: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PROJECTS:
      return {
        ...state,
        data: action.data
      }
    case GET_PROJECTS_ERROR:
      return {
        state,
        error: action.error
      }
    default:
      return state;
  }
}
