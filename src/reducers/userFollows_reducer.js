import { GET_FOLLOWERS, GET_FOLLOWERS_ERROR, GET_FOLLOWING, GET_FOLLOWING_ERROR } from 'actions/types';

const initialState = {
  followers: null,
  following: null,
  followers_error: null,
  following_error: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_FOLLOWERS:
      return {
        ...state,
        followers: action.data
      }
    case GET_FOLLOWERS_ERROR:
      return {
        state,
        followers_error: action.error
      }
    case GET_FOLLOWING:
      return {
        ...state,
        following: action.data
      }
    case GET_FOLLOWING_ERROR:
      return {
        state,
        following_error: action.error
      }
    default:
      return state;
  }
}
