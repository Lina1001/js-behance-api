import { GET_EXPERIENCE, GET_EXPERIENCE_ERROR } from 'actions/types';

const initialState = {
  data: null,
  error: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_EXPERIENCE:
      return {
        ...state,
        data: action.data
      }
    case GET_EXPERIENCE_ERROR:
      return {
        state,
        error: action.error
      }
    default:
      return state;
  }
}
