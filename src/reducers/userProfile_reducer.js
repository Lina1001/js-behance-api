import { GET_PROFILE, GET_PROFILE_ERROR } from 'actions/types';

const initialState = {
  data: null,
  error: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PROFILE:
      return {
        ...state,
        data: action.data
      }
    case GET_PROFILE_ERROR:
      return {
        state,
        error: action.error
      }
    default:
      return state;
  }
}
