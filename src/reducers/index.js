import { combineReducers } from 'redux';
import usersReducer from './users_reducer';
import userProfile from './userProfile_reducer';
import userProjects from './userProjects_reducer';
import userExperience from './userExperience_reducer';
import userFollows from './userFollows_reducer.js';

export default combineReducers({
  users: usersReducer,
  profile: userProfile,
  projects: userProjects,
  experience: userExperience,
  follows: userFollows,
})
