import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './style.css';

class UsersList extends Component {
  renderUsers() {
    if(this.props.users) {
      return this.props.users.users.map((user, i) => {
        return (
          <div className="users_list-user" key={i}>
            <Link to={`/user/${user.id}`}>
              <div className="img-container">
                <img src={user.images["138"]} alt={`${user.username}'s pic`} />
                <div className="img-overlay">View Profile</div>
              </div>
              <p>{user.username}</p>
            </Link>
          </div>
        )
      })
    }
    else if(this.props.error) {
      return <h3>Opps! Something wrong...</h3>
    }
  }
  render() {
    return (
      <div className="users_list">
        {this.renderUsers()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {users: state.users.data, error: state.users.error};
}
export default connect(mapStateToProps, null)(UsersList);
