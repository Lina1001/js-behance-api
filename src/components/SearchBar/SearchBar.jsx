import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchUsers } from 'actions/searchUsers';
import './style.css';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {queryString: ''};
  }

  componentDidMount(){
   this.searchbar.focus();
  }

  onInputChange = (e) => {
    this.setState({ queryString: e.target.value });
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.searchUsers(this.state.queryString)
    this.setState({ queryString: '' })
  }
  render() {
    return(
      <div className="search-bar">
        <h1>Welcome to Behance API App!</h1>
        <form onSubmit={this.onSubmit}>
          <div className="input-container">
            <input
              type="text"
              onChange={this.onInputChange}
              value={this.state.queryString}
              ref={(input) => { this.searchbar = input; }}
              placeholder="Enter search term, e.g X-Team"
              />
          </div>
        </form>
      </div>
    )
  }
}

export default connect(null, {searchUsers})(SearchBar);
