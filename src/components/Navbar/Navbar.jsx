import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export default () => {
  return (
    <div className="header">
      <Link to="/">HOME</Link>
      <Link to="/about">ABOUT</Link>
    </div>
  )
}
