import React from 'react';
import './style.css';
import homePageSchema from './home-page.png';
import userProfileSchema from './user-profile.png';

export default () => {
  return (
    <div className="about-wrappper">
      <h1 className="sunset-gradient-text">App Structure</h1>
      <img src={homePageSchema} alt="home page"/>
      <img src={userProfileSchema} alt="user profile"/>
    </div>
  )
}
