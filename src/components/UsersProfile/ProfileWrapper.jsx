import React from 'react';
import UserBasic from './UserBasic';
import UserProjects from './UserProjects';
import UserExperience from './UserExperience';
import UserFollows from './UserFollows';
import './style.css';

export default ({match}) => {
  return (
    <div className="profile">
      <UserBasic id={match.params.userId} />
      <UserExperience id={match.params.userId} />
      <UserProjects id={match.params.userId} />
      <UserFollows id={match.params.userId} />
    </div>
  )
}
