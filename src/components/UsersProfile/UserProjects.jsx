import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProjects } from 'actions/getProjects';
import './style.css';

class UserProjects extends Component {
  componentDidMount() {
    this.props.getProjects(this.props.id);
  }
  renderProjects() {
    if(this.props.projects) {
      const projects = this.props.projects.projects;
      return projects.map((project, i) => {
        return (
          <div className="project-item-container" key={i}>
            <a href={project.url} target="_blank">
              <div className="project-info-container">
                <div className="img-container">
                  <img src={project.covers["230"]} alt={`${project.id}'s pic`} />
                  <div className="img-overlay">View Project</div>
                </div>
                <div className="project-desc">
                  <h3>{project.name}</h3>
                  <p><i>{project.fields.toString()}</i></p>
                  <div>
                    <p>APPRECIATIONS: {project.stats.appreciations}</p>
                    <p>COMMENTS: {project.stats.comments}</p>
                    <p>VIEWS: {project.stats.views}</p>
                  </div>
                </div>
              </div>
            </a>
          </div>
        )
      });
    }
    else if(this.props.error) {
      return <h3>Oops! Something went wrong</h3>
    }
  }
  render() {
    return (
      <div className="project-wrapper">
        <h2 className="profile-section-header">Projects</h2>
        <div className="projects-container">
          {this.renderProjects()}
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {projects: state.projects.data, error: state.projects.error};
}

export default connect(mapStateToProps, { getProjects })(UserProjects)
