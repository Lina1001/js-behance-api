import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProfile } from 'actions/getProfile';
import './style.css';

class UserProfile extends Component {
  componentDidMount() {
    this.props.getProfile(this.props.id);
  }
  renderProfile() {
    if(this.props.profile) {
      const info = this.props.profile.user;
      return (
        <div className="user-basic">
          <div className="user-basic-container">
            <div className="user-basic-img-container">
              <img className="userAva" src={info.images["138"]} alt={`${info.username}'s pic`} />
            </div>
            <div className="user-basic-info-container">
              <span className="username sunset-gradient-text">{info.display_name}</span>
              <span> ({info.username})</span>
              <h3>{info.occupation} {info.company ? `@${info.company}` : ''}</h3>
              <p className="user-basic-info-location"><i>{info.location}</i></p>
              <p className="user-basic-info-focus">{info.fields.toString()}</p>
              {info.twitter ? `Twitter: ${info.twitter}` : ''}
              <div className="stats-wrapper">
                <div className="stats-left">
                  <p>APPRECIATIONS: {info.stats.appreciations}</p>
                  <p>COMMENTS: {info.stats.comments}</p>
                  <p>VIEWS: {info.stats.views}</p>
                </div>
                <div className="stats-right">
                  <p>FOLLOWERS: {info.stats.followers}</p>
                  <p>FOLLOWING: {info.stats.following}</p>
                  <p>TEAM MEMBERS: {info.stats.members ? info.stats.members : '0'}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
    else if(this.props.error) {
      return <h3>Oops! Something wrong happened!</h3>
    }
  }
  render() {
    return(
      <div>
        {this.renderProfile()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {profile: state.profile.data, error: state.profile.error};
}

export default connect(mapStateToProps, { getProfile })(UserProfile);
