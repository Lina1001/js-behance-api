import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFollowers, getFollowing } from 'actions/getFollows';
import './style.css';

function renderProfile(info) {
  return (
    info.map((user, i) => {
      return (
        <div className="follow-item-container" key={i}>
          <img src={user.images["50"]} alt={`${user.username}'s pic`} />
          <p>{user.username}</p>
        </div>
      )
    })
  )
}

class UserFollows extends Component {
  componentDidMount() {
    this.props.getFollowers(this.props.id);
    this.props.getFollowing(this.props.id);
  }
  renderFollowers() {
    if(this.props.followers) {
      return renderProfile(this.props.followers.followers);
    }
    else if(this.props.followers_error) {
      return <h3>Oops! Something wrong happened!</h3>
    }
  }

  renderFollowing() {
    if(this.props.following) {
      return renderProfile(this.props.following.following);
    }
    else if(this.props.following_error) {
      return <h3>Oops! Something wrong happened!</h3>
    }
  }

  render() {
    return(
      <div className="follows-container">
        <div className="followers-container">
          <h2 className="profile-section-header">Followers:</h2>
          <div className="followers-list">
            {this.renderFollowers()}
          </div>
        </div>
        <div className="following-container">
          <h2 className="profile-section-header">Following:</h2>
          <div className="following-list">
            {this.renderFollowing()}
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    followers: state.follows.followers,
    followers_error: state.follows.followers_error,
    following: state.follows.following,
    following_error: state.follows.following_error,
  };
}

export default connect(mapStateToProps, { getFollowers, getFollowing })(UserFollows);
