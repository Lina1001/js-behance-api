import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getExperience } from 'actions/getExperience';
import './style.css';

class UserExperience extends Component {
  componentDidMount() {
    this.props.getExperience(this.props.id)
  }
  renderExperience() {
    if(this.props.experience) {
      const experience = this.props.experience.work_experience;
      return experience.map((exp, i) => {
        return (
          <div className="work-exp-item" key={i}>
            <p>Location: {exp.location}</p>
            <p>Organization: {exp.organization}</p>
            <p>Position: {exp.position}</p>
            <p>{exp.start_date ? `Start Date: ${exp.start_date}` : '' }</p>
          </div>
        );
      });
    }
    else if(this.props.error) {
      return <h3>Oops! Something wrong happened!</h3>
    }
  }
  render() {
    return(
      <div className="experience-wrapper">
        <h2 className="profile-section-header">Work Experience</h2>
        {this.renderExperience()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {experience: state.experience.data, error: state.experience.error};
}

export default connect(mapStateToProps, { getExperience })(UserExperience);
