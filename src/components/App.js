import React from 'react';
import SearchBar from './SearchBar/SearchBar';
import UsersList from './UsersList/UsersList';

export default () => {
  return (
    <div>
      <SearchBar />
      <UsersList />
    </div>
  )
}
