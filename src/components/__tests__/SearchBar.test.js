import React from 'react';
import { mount } from 'enzyme';
import SearchBar from 'components/SearchBar/SearchBar';
import Root from 'Root';

let wrapper;

beforeEach(() => {
  wrapper = mount(
    <Root>
      <SearchBar />
    </Root>
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('has an input and a button', () => {
  expect(wrapper.find('input').length).toEqual(1);
});

describe('an input', () => {
  beforeEach(()=> {
    wrapper.find('input').simulate('change', {
      target: {
        value: 'query string'
      }
    });
    wrapper.update();
  })

  it('has an input that users can type in', () => {
    expect(wrapper.find('input').prop('value')).toEqual('query string')
  });

  it('input is cleared after form submited', () => {
    wrapper.find('form').simulate('submit');
    wrapper.update();
    expect(wrapper.find('input').prop('value')).toEqual('');
  });
})
