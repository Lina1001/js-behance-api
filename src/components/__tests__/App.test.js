import React from 'react';
import { shallow } from 'enzyme';
import App from 'components/App';
import SearchBar from 'components/SearchBar/SearchBar';
import UsersList from 'components/UsersList/UsersList';

let wrapper;

beforeEach(() => {
  wrapper = shallow(<App />);
});


it('shows a search bar', () => {
  expect(wrapper.find(SearchBar).length).toEqual(1);
});

it('shows a users list', () => {
  expect(wrapper.find(UsersList).length).toEqual(1);
});
