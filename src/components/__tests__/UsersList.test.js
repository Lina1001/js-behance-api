import React from 'react';
import { mount } from 'enzyme';
import Root from 'Root';
import UsersList from 'components/UsersList/UsersList';

const wrapper = mount(
  <Root>
    <UsersList />
  </Root>
);


it('renders correctly', () => {
  expect(wrapper.find('div.users_list').length).toEqual(1);
});
