import React from 'react';
import './styles.css';

export default () => {
  return (
    <div className="footer">
      <h3>Made with <i className="fa fa-heart" aria-hidden="true"></i> by <span className="sunset-gradient-text">Lina</span>!</h3>
    </div>
  )
}
