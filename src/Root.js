import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import reducers from 'reducers';
import ReduxPromise from 'redux-promise';
import thunk from 'redux-thunk';

export default ({ children, initialState = {} }) => {
  const middleWare = [ReduxPromise, thunk];

  const loggerMiddleware = createLogger({
    predicate: () => process.env.NODE_ENV === 'development',
  });

  middleWare.push(loggerMiddleware);

  const store = createStore(
    reducers,
    initialState,
    compose(
      applyMiddleware(...middleWare),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  )
  return (
    <Provider store={store}>
      {children}
    </Provider>
  )
}
