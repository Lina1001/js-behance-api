import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Root from 'Root';
import App from 'components/App';
import Navbar from 'components/Navbar/Navbar';
import Footer from 'components/Footer/Footer';
import About from 'components/About/About';
import UserProfile from 'components/UsersProfile/ProfileWrapper';
import './styles.css';

ReactDOM.render(
  <Root>
    <BrowserRouter>
      <div className="app-wrapper">
        <div className="header-wrapper">
          <Navbar />
        </div>
        <div className="content">
          <Switch>
            <Route exact path="/" component={App} />
            <Route path="/about" component={About} />
            <Route path="/user/:userId" component={UserProfile} />
          </Switch>
        </div>
        <div className="footer-wrapper">
          <Footer />
        </div>
      </div>
    </BrowserRouter>
  </Root>,
document.querySelector('#root'));
