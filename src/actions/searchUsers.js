import { SEARCH_USERS, SEARCH_USERS_ERROR } from './types';
import BehanceApi from 'api/index';

export function searchUsers(queryString) {
  return async (dispatch) => {
    dispatch({ type: SEARCH_USERS });
    try {
      const data = await BehanceApi.search(queryString);
      return dispatch({
        type: SEARCH_USERS,
        data
      });
    } catch (error) {
      return dispatch({
        type: SEARCH_USERS_ERROR,
        error,
      });
    }
  }
}
