import { GET_PROFILE, GET_PROFILE_ERROR } from './types';
import BehanceApi from 'api/index';

export function getProfile(id) {
  return async (dispatch) => {
    dispatch({ type: GET_PROFILE });
    try {
      const data = await BehanceApi.requestUser(id, '');
      return dispatch({
        type: GET_PROFILE,
        data
      });
    } catch (error) {
      return dispatch({
        type: GET_PROFILE_ERROR,
        error,
      });
    }
  }
}
