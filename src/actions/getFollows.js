import { GET_FOLLOWERS, GET_FOLLOWERS_ERROR, GET_FOLLOWING, GET_FOLLOWING_ERROR } from './types';
import BehanceApi from 'api/index';

export function getFollowers(id) {
  return async (dispatch) => {
    dispatch({ type: GET_FOLLOWERS });
    try {
      const data = await BehanceApi.requestUser(id, '/followers');
      return dispatch({
        type: GET_FOLLOWERS,
        data
      });
    } catch (error) {
      return dispatch({
        type: GET_FOLLOWERS_ERROR,
        error,
      });
    }
  }
}

export function getFollowing(id) {
  return async (dispatch) => {
    dispatch({ type: GET_FOLLOWING });
    try {
      const data = await BehanceApi.requestUser(id, '/following');
      return dispatch({
        type: GET_FOLLOWING,
        data
      });
    } catch (error) {
      return dispatch({
        type: GET_FOLLOWING_ERROR,
        error,
      });
    }
  }
}
