import { GET_PROJECTS, GET_PROJECTS_ERROR } from './types';
import BehanceApi from 'api/index';

export function getProjects(id) {
  return async (dispatch) => {
    dispatch({ type: GET_PROJECTS });
    try {
      const data = await BehanceApi.requestUser(id, '/projects');
      return dispatch({
        type: GET_PROJECTS,
        data
      });
    } catch (error) {
      return dispatch({
        type: GET_PROJECTS_ERROR,
        error,
      });
    }
  }
}
