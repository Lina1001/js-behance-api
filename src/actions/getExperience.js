import { GET_EXPERIENCE, GET_EXPERIENCE_ERROR } from './types';
import BehanceApi from 'api/index';

export function getExperience(id) {
  return async (dispatch) => {
    dispatch({ type: GET_EXPERIENCE });
    try {
      const data = await BehanceApi.requestUser(id, '/work_experience');
      return dispatch({
        type: GET_EXPERIENCE,
        data
      });
    } catch (error) {
      return dispatch({
        type: GET_EXPERIENCE_ERROR,
        error,
      });
    }
  }
}
