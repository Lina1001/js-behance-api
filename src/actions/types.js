export const SEARCH_USERS = 'search_users';
export const SEARCH_USERS_ERROR = 'search_users_error';

export const GET_PROFILE = 'get_profile';
export const GET_PROFILE_ERROR = 'get_profile_error';

export const GET_PROJECTS = 'get_projects';
export const GET_PROJECTS_ERROR = 'get_projects_error';

export const GET_EXPERIENCE = 'get_experience';
export const GET_EXPERIENCE_ERROR = 'get_experience_error';

export const GET_FOLLOWERS = 'get_followers';
export const GET_FOLLOWERS_ERROR = 'get_followers_error';

export const GET_FOLLOWING = 'get_following';
export const GET_FOLLOWING_ERROR = 'get_following_error';
