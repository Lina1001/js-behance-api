import axios from 'axios';

class BehanceApi {
  async search(queryString) {
    try {
      const { data } = await axios.post('/users', {queryString});
        return data;
      } catch (e) {
        return e;
      }
  }
  async requestUser(id, type) {
    try {
      const { data } = await axios.post('/user', {id, type});
        return data;
      } catch (e) {
        return e;
      }
  }
}

export default new BehanceApi();
