const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const request = require('request');
const app = express();
app.use(bodyParser.json())

const API_KEY = 'Z7CUol5R6VLHy5Tomzao3cgZm7fkJOae';
const BEHANCE_API_URL = 'https://api.behance.net/v2/users';

app.use(express.static(path.join(__dirname, 'build')));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.post('/users', function(req, res) {
  const queryString = req.body.queryString;
  request(`${BEHANCE_API_URL}?q=${queryString}&api_key=${API_KEY}`, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.send(body)
    }
  })
})

app.post('/user', function(req, res) {
  const id = req.body.id;
  const type = req.body.type;
  request(`${BEHANCE_API_URL}/${id}${type}?api_key=${API_KEY}`, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.send(body)
    }
  })
})

const PORT = process.env.PORT || 8081;

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});
