# Front End Code Challenge

## The Challenge
Behance is one of the most popular websites for front-end and art portfolios. For this code challenge, you'll be using the Behance JSON API to build out a quick front-end for a search users and profile display website. To get started, visit here: Behance Developers.

The first view of the app should have a search box where searches for users are executed
When a user is chosen/found, the next view is an individual profile page for the user. The profile page should include:

- [x] basic information about the user (including stats)
- [x] a list of their projects with links to them (links should be exterior to your app and link directly to Behance)
- [x] a list of their work experience,
- [x] a preview of their followers and following lists.

## How to view project:
1. npm install
2. In separate tab run "node server"
3. And in another tab run "npm start"
### Great! Now you can take a look at project at localhost://3000 :tada:

## How to test project:
1. npm run test

** Made with :heart: by Lina **
